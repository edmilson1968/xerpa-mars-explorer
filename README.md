# mars-explorer-shell

## fase 1 - aplicação console
O projeto utiliza springboot 2.1.x e está com uma configuração bem vanilla. 

A idéia básica foi criar uma espécie de conteiner de sondas, onde ficariam delimitadas geograficamente. Dei inicio a isso criando o objeto Terrain que delimita um planeta ou área geográfica tornando possível pousar "Probes" neste, e facilitando o controle de colisões e bordas.

Não há muito o que dizer sobre a organização dos pacotes, uma vez que optei pelo modelo padrão, com as entidades Probe e Terrain, um único ProbeService que contém as regras de negócio para "pousar" uma sonda, os Enums que formam a sua máquina de estados e também não podia faltar o pacote de Exceptions. Procurei criar uma para cada evento que achei relevante.

Também protegi alguns setters tanto da classe Probe quanto da classe Terrain para evitar "saltos de sondas"
e "expansões geográficas" inapropriados.

Faltava ainda escolher como seria o controle das ações da sonda. Eu gostaria de fazer algo compacto, bem determinado.

Optei por realizar o controle da navegação utilizando uma máquina de estados implementada por Enums, que facilitou enormemente a implementação dos comandos enviados à sonda. 

Estes Enums são:

* Moves: recebe as ações que a sonda realizará (L, R ou M)
* Directions: determina a "rosa-dos-ventos" da sonda. Aceita os parâmetros N, S, W e E - correspondentes às direções onde a sonda aponta e é controlada pela máquina de estados através das ações vindas pelo método "next" (acionado pelo enum Move);

show me the code..

## clonando e compilando
```shell
$ https://gitlab.com/edmilson1968/xerpa-mars-explorer.git
$ cd xerpa-mars-explorer
$ mvn clean install
```

## executando
A entrada dos dados se dá por um arquivo texto, criado no mesmo modelo da descrição do problema, passado como parâmetro para o arquivo .jar.

```shell
$ mvn spring-boot:run -Dspring-boot.run.arguments=<caminho do arquivo de entrada>

ou então

$ java -jar <caminho .jar> <caminho do arquivo de entrada>

```

*exemplo:*

```shell
$ mvn spring-boot:run -Dspring-boot.run.arguments=/tmp/input.txt

ou então

$ java -jar target/xerpa-marsprobe-0.0.1-SNAPSHOT.jar input.txt

```

That's all folks!