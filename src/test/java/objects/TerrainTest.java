package objects;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.marsprobe.entities.Probe;
import br.com.marsprobe.entities.Terrain;
import br.com.marsprobe.enums.Directions;

@RunWith(SpringRunner.class)
public class TerrainTest {

	Terrain terrain;
	List<Probe> lista;
	
	@Before
	public void setUp() throws Exception {
		terrain = new Terrain(5, 5);
		lista = new ArrayList<>();
		terrain.addProbe(new Probe(1, 2, Directions.N, terrain));
		terrain.addProbe(new Probe(3, 3, Directions.E, terrain));
	}

	@Test
	public void testAddProbe() {
		assertThat(terrain.addProbe(new Probe(1, 2, Directions.N, terrain))).isEqualTo(3);
		assertThat(terrain.addProbe(new Probe(1, 2, Directions.N, terrain))).isEqualTo(4);
	}

	@Test
	public void testNoCollision() {
		assertThat(terrain.collision(3, 4)).isEqualTo(false);
		assertThat(terrain.collision(0, 5)).isEqualTo(false);
	}

	@Test
	public void testCollision() {
		assertThat(terrain.collision(1, 2)).isEqualTo(true);
		assertThat(terrain.collision(3, 3)).isEqualTo(true);
	}

}
