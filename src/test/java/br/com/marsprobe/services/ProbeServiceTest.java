package br.com.marsprobe.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.marsprobe.entities.Probe;
import br.com.marsprobe.entities.Terrain;
import br.com.marsprobe.enums.Directions;
import br.com.marsprobe.exceptions.CollisionProbeException;
import br.com.marsprobe.exceptions.DirectionProbeException;
import br.com.marsprobe.exceptions.MoveProbeException;
import br.com.marsprobe.exceptions.OutOfMapProbeException;

@RunWith(SpringRunner.class)
public class ProbeServiceTest {

	@MockBean
	private Terrain terrain;
	
	@InjectMocks
	private ProbeService probeServiceMock;
	
	private Terrain mars;
	
	@Before
	public void setUp() throws Exception {
		mars = new Terrain(5, 5);
		when(terrain.getMaxX()).thenReturn(mars.getMaxX());
		when(terrain.getMaxY()).thenReturn(mars.getMaxY());
		when(terrain.getMinX()).thenReturn(mars.getMinX());
		when(terrain.getMinY()).thenReturn(mars.getMinY());
		when(terrain.addProbe(any(Probe.class))).thenReturn(1);
	}

	@Test
	public void testLandingProbe() throws CollisionProbeException, OutOfMapProbeException, DirectionProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);
		int x = 1;
		int y = 2;
		Directions dir = Directions.N;
		Probe p = probeServiceMock.landProbe(x, y, dir.name(), terrain);
		assertThat(p.getId()).isEqualTo(1);
		assertThat(p.getMaxX()).isEqualTo(mars.getMaxX());
		assertThat(p.getMaxY()).isEqualTo(mars.getMaxY());
		assertThat(p.getMinX()).isEqualTo(mars.getMinX());
		assertThat(p.getMinY()).isEqualTo(mars.getMinY());
		assertThat(p.getMaxX()).isEqualTo(mars.getMaxX());
		assertThat(p.getX()).isEqualTo(x);
		assertThat(p.getY()).isEqualTo(y);
		assertThat(p.getDirection()).isEqualTo(dir);
	}

	@Test
	public void testLandingProbeUnknownDirection() throws CollisionProbeException, OutOfMapProbeException, DirectionProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);
		assertThatThrownBy(() -> probeServiceMock.landProbe(0, 0, "Z", terrain)).isInstanceOf(DirectionProbeException.class);
	}
	
	@Test
	public void testLandingProbeWithCollision() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(true);
		assertThatThrownBy(() -> probeServiceMock.landProbe(1, 2, "N", terrain) ).isInstanceOf(CollisionProbeException.class);
	}
	
	@Test
	public void testLandingProbeOutOfMap() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);
		assertThatThrownBy(() -> probeServiceMock.landProbe(6, 2, "N", mars) ).isInstanceOf(OutOfMapProbeException.class);
		assertThatThrownBy(() -> probeServiceMock.landProbe(1, 7, "W", mars) ).isInstanceOf(OutOfMapProbeException.class);
		assertThatThrownBy(() -> probeServiceMock.landProbe(8, 8, "E", mars) ).isInstanceOf(OutOfMapProbeException.class);
	}
	
	@Test
	public void testHandleMoves() throws CollisionProbeException, OutOfMapProbeException, MoveProbeException, DirectionProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe opportunity = new Probe(1, 2, Directions.N, mars);
		mars.addProbe(opportunity);
		
		assertThat(probeServiceMock.handleMoves(opportunity, "LMLMLMLMM")).isEqualTo("1 3 N");

		Probe spirit = new Probe(3, 3, Directions.E, mars);
		assertThat(probeServiceMock.handleMoves(spirit, "MMRMMRMRRM")).isEqualTo("5 1 E");
	}

	@Test
	public void testHandleWrongMove() throws CollisionProbeException, OutOfMapProbeException, MoveProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe opportunity = new Probe(1, 2, Directions.N, mars);
		mars.addProbe(opportunity);
		
		assertThatThrownBy(() -> probeServiceMock.handleMoves(opportunity, "LMRT") ).isInstanceOf(MoveProbeException.class);
	}

	@Test
	public void testCollision() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(true);
		
		Probe beagle2 = new Probe(1, 2, Directions.N, terrain);
//		mars.addProbe(beagle2);
		
		assertThatThrownBy(() -> probeServiceMock.handleMoves(beagle2, "LMLMLMLMM")).isInstanceOf(CollisionProbeException.class);
	}
	
	@Test
	public void testMoveOutOfTerrainToEast() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe toEast = new Probe(3, 3, Directions.E, terrain);
		assertThatThrownBy(() -> probeServiceMock.handleMoves(toEast, "MMM")).isInstanceOf(OutOfMapProbeException.class);
	}
	@Test
	public void testMoveOutOfTerrainToNorth() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe toNorth = new Probe(3, 3, Directions.N, terrain);
		assertThatThrownBy(() -> probeServiceMock.handleMoves(toNorth, "MMM")).isInstanceOf(OutOfMapProbeException.class);
	}		
	@Test
	public void testMoveOutOfTerrainToSouth() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe toSouth = new Probe(3, 3, Directions.S, terrain);
		assertThatThrownBy(() -> probeServiceMock.handleMoves(toSouth, "MMMM")).isInstanceOf(OutOfMapProbeException.class);
	}
	@Test
	public void testMoveOutOfTerrainToWest() throws CollisionProbeException, OutOfMapProbeException {
		when(terrain.collision(any(int.class), any(int.class))).thenReturn(false);

		Probe toWest = new Probe(3, 3, Directions.W, terrain);
		assertThatThrownBy(() -> probeServiceMock.handleMoves(toWest, "MMMM")).isInstanceOf(OutOfMapProbeException.class);
	}
	
}
