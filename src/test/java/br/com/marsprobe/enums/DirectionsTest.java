package br.com.marsprobe.enums;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import br.com.marsprobe.enums.Directions;
import br.com.marsprobe.enums.Moves;

public class DirectionsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testNextCompass() {
		Directions dir = Directions.N;
		assertThat(dir.next(Moves.M)).isEqualTo(Directions.N);
		
		assertThat(dir.next(Moves.L).next(Moves.R)).isEqualTo(Directions.N);
		
		assertThat(dir.next(Moves.L)).isEqualTo(Directions.W);
		assertThat(dir.next(Moves.L).next(Moves.L)).isEqualTo(Directions.S);
		assertThat(dir.next(Moves.L).next(Moves.L).next(Moves.L)).isEqualTo(Directions.E);
		assertThat(dir.next(Moves.L).next(Moves.L).next(Moves.L).next(Moves.L)).isEqualTo(Directions.N);

		assertThat(dir.next(Moves.R)).isEqualTo(Directions.E);
		assertThat(dir.next(Moves.R).next(Moves.R)).isEqualTo(Directions.S);
		assertThat(dir.next(Moves.R).next(Moves.R).next(Moves.R)).isEqualTo(Directions.W);
		assertThat(dir.next(Moves.R).next(Moves.R).next(Moves.R).next(Moves.R)).isEqualTo(Directions.N);

		assertThat(dir.next(Moves.R)).isEqualTo(Directions.E);
		assertThat(dir.next(Moves.R).next(Moves.M)).isEqualTo(Directions.E);
		assertThat(dir.next(Moves.R).next(Moves.M).next(Moves.R)).isEqualTo(Directions.S);
		assertThat(dir.next(Moves.L).next(Moves.L).next(Moves.M).next(Moves.L)).isEqualTo(Directions.E);
	}

}
