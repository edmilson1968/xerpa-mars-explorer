package br.com.marsprobe;

import br.com.marsprobe.entities.Terrain;
import br.com.marsprobe.exceptions.CollisionProbeException;
import br.com.marsprobe.exceptions.OutOfMapProbeException;
import br.com.marsprobe.services.ProbeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class ApplicationTests {

    private ProbeService probeService;
    private Application application;
    private Path workingDir;
    ClassLoader classLoader;

    @Before
    public void setup() {
        classLoader = getClass().getClassLoader();
        workingDir = Paths.get("src","test","resources");
        probeService = Mockito.mock(ProbeService.class);
        application = new Application();

        ReflectionTestUtils.setField(application, "probeService", probeService);
    }

    @Test(expected = RuntimeException.class)
    public void shouldInitWithNoArgs() throws Exception {
        application.run();
    }

    @Test
    public void shouldRunWithZeroLineFile() throws Exception {
        final String empty = new File(classLoader.getResource("empty").getFile()).getAbsolutePath();
        application.run(empty);
        Terrain mars = (Terrain) ReflectionTestUtils.getField(application, "mars");
        assertThat(mars).isNull();
        verify(probeService, times(0)).landProbe(anyInt(), anyInt(), anyString(), any(Terrain.class));
    }

    @Test
    public void shouldRunWithOneLineFile() throws Exception {
        final String one = new File(classLoader.getResource("one").getFile()).getAbsolutePath();
        application.run(one);
        Terrain mars = (Terrain) ReflectionTestUtils.getField(application, "mars");
        assertThat(mars).isNotNull();
        assertThat(mars).isEqualTo(new Terrain(5, 5));
        verify(probeService, times(0)).landProbe(anyInt(), anyInt(), anyString(), any(Terrain.class));
    }

    @Test
    public void shouldRunWithTwoLineFile() throws Exception {
        final String two = new File(classLoader.getResource("two").getFile()).getAbsolutePath();
        application.run(two);
        Terrain mars = (Terrain) ReflectionTestUtils.getField(application, "mars");
        assertThat(mars).isNotNull();
        assertThat(mars).isEqualTo(new Terrain(5, 5));
        verify(probeService, times(1)).landProbe(1, 2, "N", mars);
    }

    @Test
    public void shouldThrowsCollisionProbeException() throws Exception {
        final String two = new File(classLoader.getResource("two").getFile()).getAbsolutePath();
        when(probeService.handleMoves(any(), anyString())).thenThrow(CollisionProbeException.class);
        application.run(two);
    }

    @Test
    public void shouldThrowsOutOfMapProbeException() throws Exception {
        final String two = new File(classLoader.getResource("two").getFile()).getAbsolutePath();
        when(probeService.handleMoves(any(), anyString())).thenThrow(OutOfMapProbeException.class);
        application.run(two);
    }

}

