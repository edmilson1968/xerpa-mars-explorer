package br.com.marsprobe.services;

import org.springframework.stereotype.Service;

import br.com.marsprobe.entities.Probe;
import br.com.marsprobe.entities.Terrain;
import br.com.marsprobe.enums.Directions;
import br.com.marsprobe.enums.Moves;
import br.com.marsprobe.exceptions.CollisionProbeException;
import br.com.marsprobe.exceptions.DirectionProbeException;
import br.com.marsprobe.exceptions.MoveProbeException;
import br.com.marsprobe.exceptions.OutOfMapProbeException;

@Service
public class ProbeService {

	public ProbeService() {	}

	public Probe landProbe(int x, int y, String dir, Terrain terrain) throws CollisionProbeException, OutOfMapProbeException, DirectionProbeException {

		if (terrain.collision(x, y)) {
			throw new CollisionProbeException(
			String.format("ocorreu uma colisão ao tentar pousar nas coordenadas (%d,%d)",
				x, y)
			);
		}
		
		//fora dos limites do planeta
		if (x > terrain.getMaxX() || y > terrain.getMaxY())
			throw new OutOfMapProbeException(
			String.format("tentativa de pouso fora dos limites do planeta nas coordenadas (%d,%d)",
				x, y) 
			);
		
		Directions adir;
		try {
			adir = Directions.valueOf(dir);
		} catch (IllegalArgumentException e) {
			throw new DirectionProbeException("nao há uma direção disponível");
		}
		
		Probe p = new Probe(x, y, adir, terrain);
		p.setId(terrain.addProbe(p));
		
		return p;
	}
	
	public String handleMoves(Probe p, String move) throws CollisionProbeException, OutOfMapProbeException, MoveProbeException, DirectionProbeException {
			
		for (Character m: move.toUpperCase().toCharArray()) {
			
			Moves where;
			try {
				where = Moves.valueOf(String.valueOf(m));
			} catch (IllegalArgumentException e) {
				throw new MoveProbeException(
					String.format(
					"comando de movimento inválido na string (%s). Somente L, R ou M permitidos",
							move)
					);
			}
			
			if (where.equals(Moves.M)) {
				p.moveAhead();
			} else {
				p.setDirection(p.getDirection().next(where));
			}
		}
		
		return String.format("%d %d %s", p.getX(), p.getY(), p.getDirection());
	}
	
}
