package br.com.marsprobe.exceptions;

public class DirectionProbeException extends Exception {

	private static final long serialVersionUID = 1726440548126818577L;

	public DirectionProbeException(String message) {
		super(message);
	}

}
