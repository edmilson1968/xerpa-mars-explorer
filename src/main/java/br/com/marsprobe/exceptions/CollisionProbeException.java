package br.com.marsprobe.exceptions;

public class CollisionProbeException extends Exception {

	private static final long serialVersionUID = -3619566064016915029L;

	public CollisionProbeException(String message) {
		super(message);
	}

}
