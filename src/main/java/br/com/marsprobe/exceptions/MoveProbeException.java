package br.com.marsprobe.exceptions;

public class MoveProbeException extends Exception {

	private static final long serialVersionUID = -7139676081095731618L;

	public MoveProbeException(String message) {
		super(message);
	}

}
