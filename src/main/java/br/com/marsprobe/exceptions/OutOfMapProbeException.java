package br.com.marsprobe.exceptions;

public class OutOfMapProbeException extends Exception {

	private static final long serialVersionUID = -3619566064016915029L;

	public OutOfMapProbeException(String message) {
		super(message);
	}

}
