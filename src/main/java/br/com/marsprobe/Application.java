package br.com.marsprobe;

import java.io.File;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.marsprobe.entities.Probe;
import br.com.marsprobe.entities.Terrain;
import br.com.marsprobe.exceptions.CollisionProbeException;
import br.com.marsprobe.exceptions.OutOfMapProbeException;
import br.com.marsprobe.services.ProbeService;

@SpringBootApplication
public class Application implements CommandLineRunner {

	private static Logger LOG = LoggerFactory
		      .getLogger(Application.class);


	@Autowired
	private ProbeService probeService;
	private Terrain mars;


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		

		if (args.length == 0) {
			throw new RuntimeException("é necessário executar o programa com um arquivo de dados como parâmetro de entrada");
		}
		
		LOG.info("abrindo arquivo de dados: {}", args[0]);
		Scanner scanner = new Scanner(new File(args[0]));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.matches("[0-9]+ [0-9]+")) { //configuracao do planeta
				String[] planalto = line.split(" ");
				int maxX = Integer.parseInt(planalto[0]);
				int maxY = Integer.parseInt(planalto[1]);
				mars = new Terrain(maxX, maxY);
			} else if (line.matches("[0-9]+ [0-9]+ [N|S|W|E]")) { //configuracao de uma sonda
				String[] sonda = line.split(" ");
				int x = Integer.parseInt(sonda[0]);
				int y = Integer.parseInt(sonda[1]);
				try {
					//pousa a sonda
					Probe p = probeService.landProbe(x, y, sonda[2], mars);
					//busca configuração dos comandos da sonda
					line = scanner.nextLine();
					if (line.matches("[L|R|M]+")) {
						String res = probeService.handleMoves(p, line);
						LOG.info("Resultado para sonda {}: {}", p.getId(), res);
					}
				} catch (CollisionProbeException e) {
					LOG.error(e.getMessage());
				} catch (OutOfMapProbeException e) {
					LOG.error(e.getMessage());
				} catch (Exception e) {
					LOG.error(e.getMessage());
				}
			}
		}

		scanner.close();
	}

}

