package br.com.marsprobe.entities;

import java.util.ArrayList;
import java.util.List;

public class Terrain {

	private int minX, minY;
	private int maxX, maxY;
	
	private List<Probe> probes = new ArrayList<>();
	
	public Terrain(int maxX, int maxY) {
		super();
		
		//por definição
		this.minX = 0;
		this.minY = 0;
		//define o máximo do mapa
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	public int getMinX() {
		return minX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public int addProbe(Probe p) {
		probes.add(p);
		return probes.size();
	}
	
	public boolean collision(int x, int y) {
		if (probes.isEmpty())
			return false;
		else {
			for (Probe p: probes) {
				if (p.getX() == x && p.getY() == y)
					return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Terrain terrain = (Terrain) o;

		if (minX != terrain.minX) return false;
		if (minY != terrain.minY) return false;
		if (maxX != terrain.maxX) return false;
		if (maxY != terrain.maxY) return false;
		return probes != null ? probes.equals(terrain.probes) : terrain.probes == null;
	}

	@Override
	public int hashCode() {
		int result = minX;
		result = 31 * result + minY;
		result = 31 * result + maxX;
		result = 31 * result + maxY;
		result = 31 * result + (probes != null ? probes.hashCode() : 0);
		return result;
	}
}
