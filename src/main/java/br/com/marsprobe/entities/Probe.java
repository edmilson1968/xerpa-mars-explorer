package br.com.marsprobe.entities;

import br.com.marsprobe.enums.Directions;
import br.com.marsprobe.exceptions.CollisionProbeException;
import br.com.marsprobe.exceptions.MoveProbeException;
import br.com.marsprobe.exceptions.OutOfMapProbeException;

public class Probe {

	private int id = 0;
	private int x, y;
	private Directions direction;
	private Terrain terrain;
	
	public Probe(int x, int y, Directions dir, Terrain terrain) {
		this.x = x;
		this.y = y;
		
		direction = dir;
		this.terrain = terrain;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getMaxX() {
		return terrain.getMaxX();
	}

	public int getMaxY() {
		return terrain.getMaxX();
	}

	public int getMinX() {
		return terrain.getMinX();
	}

	public int getMinY() {
		return terrain.getMinY();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Directions getDirection() {
		return direction;
	}

	public void setDirection(Directions direction) {
		this.direction = direction;
	}

	public Terrain getTerrain() {
		return terrain;
	}

	public void moveAhead() 
			throws CollisionProbeException, OutOfMapProbeException, MoveProbeException {
		
		int[] newcoord = translateMoveAhead();
		
		if (! terrain.collision(newcoord[0], newcoord[1])) {
			x =  newcoord[0];
			y = newcoord[1];
		} else {
			throw new CollisionProbeException(
				String.format("ocorrerá uma colisão ao tentar chegar nas coordenadas (%d,%d)",
						newcoord[0], newcoord[1]));
		}
	}
	
	private int[] translateMoveAhead() throws OutOfMapProbeException {
		int x = this.getX();
		int y = this.getY();
		
		switch (this.getDirection()) {
		case N:
			if (y == this.getMaxY())
				throw new OutOfMapProbeException(
					String.format("a sonda saiu do mapa pelo Norte, coordenadas (%d,%d)", x, y));
			y++;
			break;
		case W:
			if (x == this.getMinX())
				throw new OutOfMapProbeException(
					String.format("a sonda saiu do mapa pelo Oeste, coordenadas (%d,%d)", x, y));
			x--;
			break;
		case S:
			if (y == this.getMinY())
				throw new OutOfMapProbeException(
					String.format("a sonda saiu do mapa pelo Sul, coordenadas (%d,%d)", x, y));
			y--;
			break;
		case E:
			if (x == this.getMaxX())
				throw new OutOfMapProbeException(
					String.format("a sonda saiu do mapa pelo Leste, coordenadas (%d,%d)", x, y));
			x++;
			break;
			
		default:
			break;
		}

		return new int[] {x, y};
	}
	
}
