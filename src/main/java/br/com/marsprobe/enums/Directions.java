package br.com.marsprobe.enums;

public enum Directions {

    E() {
        @Override
        public Directions next(Moves move) {
            return move == Moves.R ? S : move == Moves.L ? N : E;
        }
    },
    W() {
        @Override
        public Directions next(Moves move) {
            return move == Moves.R ? N : move == Moves.L ? S : W;
        }
    },
    S() {
        @Override
        public Directions next(Moves move) {
            return move == Moves.R ? W : move == Moves.L ? E : S;
        }
    },
    N() {
        @Override
        public Directions next(Moves move) {
            return move == Moves.R ? E : move == Moves.L ? W : N;
        }
    };

    public abstract Directions next(Moves rotation);

}